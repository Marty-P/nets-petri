import numpy

class NetPetri:
	def __init__(self, ft, fp, null_pos, ds):
		if self.check_correct_matrix(ft, fp):
			self.ft = ft
			self.fp = fp
			self.num_p = numpy.shape(fp)[0]
			self.num_t = numpy.shape(fp)[1]
			self.depth_search = ds
			self.tree = Tree(null_pos)
			self.list_possitions = Possitions([null_pos], [self.tree.num_possition])
			self.access = True
			self.tree = self.search_tree(self.tree)
			#self.print_tree(self.tree)
		else:
			self.access = False
	
	
	def check_correct_matrix(self, ft, fp):
		return numpy.shape(ft) == numpy.shape(fp)
	
	
	def search_tree(self, tree, depth=0):
		if tree.unlock and (depth < self.depth_search):
			list_branches = self.calc_new_possitions(tree.value)
			list_branches = self.check_unique_position(list_branches)
			tree = self.construct_tree(list_branches, tree, depth+1)
			for i in range(0, len(tree.branches)):
				tree.branches[i] = self.search_tree(tree.branches[i], depth+1)
		return tree
			

	def check_unique_position(self, list):
		for i in range(0, list.amt_elem):
			if self.list_possitions.get_num_possition(list.values[i]):
				list.repetition[i] = True
				list.nums_possitions[i] = self.list_possitions.get_num_possition(list.values[i])
			else:
				list.nums_possitions[i] = 'm'+str(self.list_possitions.amt_elem+1)
				self.list_possitions.append(list.values[i], self.list_possitions.amt_elem+1)	
		return list
				
	def construct_tree(self, list, tree, depth):
		for i in range(0, list.amt_elem):
			tree.addBranches(Tree(list.values[i], list.transitions[i], list.nums_possitions[i], not list.repetition[i], depth))
		return tree
		
		
	#calculating positions at the same level	
	def calc_new_possitions(self, value):
		list = Branches()
		new_possition = numpy.empty(self.num_p)
		for i in range(0, self.num_t):
			new_possition = value - self.ft[:,i]
			if numpy.min(new_possition) >= 0:
				new_possition = new_possition + self.fp[:,i]
				list.append(new_possition, i+1)
		return list
		
	def print_tree(self, tree):
		print(tree)
		for i in tree.branches:
			self.print_tree(i)
			
	def get_tree_str(self):
		ln = len('+ ' + str(self.tree.num_possition) + '  ' + self.tree.transition + str(self.tree.value)) + 2
		txt =  self.construt_tree_str(self.tree, ln)
		return txt
	
	def construt_tree_str(self, tree, ln, depth=0):
		txt = ''
		
		for j in range(0, depth-1):
			for i in range(0, ln):
				txt = txt + ' '
		if depth:
			txt = txt + '|'
			for i in range(0, ln-1):
				txt = txt + '-'
			
		txt = txt + '. ' + tree.transition + ' ' + str(tree.num_possition) + str(tree.value)
		if not tree.unlock:
			txt = txt + ' повтор'
		txt = txt + '\n'

			
		for i in tree.branches:
			txt = txt + self.construt_tree_str(i, ln, depth+1)
		return txt
		
	
			
class Branches:
	def __init__(self):
		self.values = []
		self.transitions = []
		self.nums_possitions = []
		self.repetition = []
		self.amt_elem = 0
	
	def append(self, value, transition):
		self.values.append(value)
		self.transitions.append('t' + str(transition))
		self.nums_possitions.append(None)
		self.repetition.append(False)
		self.amt_elem =  self.amt_elem + 1

class Possitions:
	def __init__(self, value, num_pos):
		self.values = [value]
		self.nums_possitions = [num_pos]
		self.amt_elem = 0
		
		
	def append(self, value, num_pos):
		self.values.append(value)
		self.nums_possitions.append('M'+str(num_pos))
		self.amt_elem = self.amt_elem + 1
		
		
	def get_num_possition(self, value):
		num_pos = False
		for i in range(0, self.amt_elem):
			if numpy.array_equal(value, self.values[i]):
				num_pos = self.nums_possitions[i]
				break
		return num_pos
				
		

class Tree:
	def __init__(self, vle, tr='', num_p='m0', ulc=True, lvl=0):
		self.value = vle
		self.branches = []
		self.transition = tr
		self.num_possition = num_p
		self.level = lvl
		self.unlock = ulc
	
	def __str__(self):
		return str(self.value)	
		
	def addBranches(self, branche):
		self.branches.append(branche)
		
		
			
def log(str):
	f = open('log.txt', 'a')
	f.write(str+'\n')
	f.close()
			
			
			